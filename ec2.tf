resource "aws_security_group" "test1" {
  name        = "tf-test1"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      # allow port 80 everywhere
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      # allow ssh only from tpt
      "193.40.160.15/32",
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  count = 5

  ami           = "ami-1dab2163" # ubuntu
  instance_type = "t3.nano"

  # name of the access key - create one in EC2 -> Key Pairs
  # https://eu-north-1.console.aws.amazon.com/ec2/v2/home?region=eu-north-1
  key_name = "tpt2"

  security_groups  = [
      aws_security_group.test1.name
  ]

  associate_public_ip_address = true

  tags = {
    Name = "tf-test-nano"
  }

  root_block_device {
    volume_size = 12
  }

  user_data = file("${path.module}/userdata.sh")
}

output "ec2" {
  # if you want to ssh any machine, use
  # ssh -i keyname.pem ubunut@123.123.123.123  (replace with your IP)
  value = aws_instance.web.*.public_ip
}
