# you can see Load Balancers under EC2 -> Load Balancers
# https://eu-north-1.console.aws.amazon.com/ec2/v2/home?region=eu-north-1
resource "aws_elb" "test" {
  name               = "tf-test"
  availability_zones = ["eu-north-1a", "eu-north-1b"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  # this lb will listen all our machines created with terraform
  instances                   = aws_instance.web.*.id

  tags = {
    Name = "tf-test"
  }
}

output "lb" {
  value = "http://${aws_elb.test.dns_name}/"
}